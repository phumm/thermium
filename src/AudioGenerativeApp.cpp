#include "cinder/app/AppBasic.h"
#include "cinder/audio/Output.h"
#include "cinder/audio/Callback.h"
#include "cinder/CinderMath.h"
#include "cinder/gl/Texture.h"
#include "cinder/Text.h"
#include "cinder/Utilities.h"
#include "cinder/ImageIo.h"
#include "cinder/Font.h"

using namespace ci;
using namespace ci::app;
using namespace std;

#include <list>
using std::list;

static const bool PREMULT = false;


/*
void printFontNames()
{
	for( vector<string>::const_iterator fontName = Font::getNames().begin(); fontName != Font::getNames().end(); ++fontName )
		console() << *fontName << endl;
}
 */



// This line tells Cinder to actually create the application
//CINDER_APP_NATIVE( TextTestApp, RendererGl )

//  text ab hier

class AudioGenerativeApp : public AppBasic {
 public:
    
    void setup_text();
	void draw_text();
    gl::Texture	mTexture;//, mSimpleTexture;
    
	void setup();
	void mouseMove( MouseEvent event );
	void sineWave( uint64_t inSampleOffset, uint32_t ioSampleCount, audio::Buffer32f *ioBuffer );
	void draw();
	
	float mFreqTarget;
	float mPhase;
	float mPhaseAdjust;
	float mMaxFreq;
};


void AudioGenerativeApp::draw_text()
{
	// this pair of lines is the standard way to clear the screen in OpenGL
	glClearColor( 0.1f, 0.1f, 0.1f, 1.0f );
	glClear( GL_COLOR_BUFFER_BIT );
	gl::setMatricesWindow( getWindowSize() );
    
	gl::enableAlphaBlending( PREMULT );
    
	gl::color( Color::white() );
	gl::draw( mTexture, Vec2f( 10, 10 ) );
    //gl::draw( mSimpleTexture, Vec2f( 10, getWindowHeight() - mSimpleTexture.getHeight() - 5 ) );
}


void AudioGenerativeApp::setup_text()
{
	//printFontNames();
    
#if defined( CINDER_COCOA_TOUCH )
	std::string normalFont( "Arial" );
    //	std::string boldFont( "Arial-BoldMT" );
    //	std::string differentFont( "AmericanTypewriter" );
#else
	std::string normalFont( "HelveticaNeue-Bold" );
    //	std::string boldFont( "Arial Bold" );
    //	std::string differentFont( "Papyrus" );
#endif
    
	// this does a complicated layout
	TextLayout layout;
	layout.clear( ColorA( 0.2f, 0.2f, 0.2f, 0.2f ) );
	layout.setFont( Font( normalFont, 24 ) );
	layout.setColor( Color( 1, 1, 1 ) );
	layout.addLine( std::string( "Unicode: " ));
	Surface8u rendered = layout.render( true, PREMULT );
	mTexture = gl::Texture( rendered );
}

// audio ab hier..

void AudioGenerativeApp::setup()
{
	mMaxFreq = 2500.0f;
	mFreqTarget = 0.0f;
	mPhase = 0.0f;
	mPhaseAdjust = 0.0f;
	
	audio::Output::play( audio::createCallback( this, &AudioGenerativeApp::sineWave ) );
}

void AudioGenerativeApp::mouseMove( MouseEvent event )
{
	int height = getWindowHeight();
	//gFreqTarget = ( height - event.getY() ) / (float)height * gMaxFreq/ 44100.0f;
	mFreqTarget = math<float>::clamp( ( height - event.getY() ) / (float)height * mMaxFreq, 0.0, mMaxFreq );
}

void AudioGenerativeApp::sineWave( uint64_t inSampleOffset, uint32_t ioSampleCount, audio::Buffer32f *ioBuffer ) {
	mPhaseAdjust = mPhaseAdjust * 0.95f + ( mFreqTarget / 44100.0f ) * 0.05f;
	for( int  i = 0; i < ioSampleCount; i++ ) {
		mPhase += mPhaseAdjust;
       // Log("array : %@",mPhase);
		mPhase = mPhase - math<float>::floor( mPhase );
		float val = math<float>::sin( mPhase * 2.0f * M_PI );
		
		ioBuffer->mData[i*ioBuffer->mNumberChannels] = val;
		ioBuffer->mData[i*ioBuffer->mNumberChannels + 1] = val;
	}
}

void AudioGenerativeApp::draw()
{
	gl::clear( Color( 0.1f, 0.1f, 0.1f ) );
    
	// draw a representation of the sine wave
	const float audioToVisualScale = 10.0f;
	const float waveHeight = 200.0f;
	gl::color( Color( 1.0f, 0.5f, 0.25f ) );
//    gl::color( Color::white() );
	glBegin( GL_LINE_STRIP );
	for( float x = -getWindowWidth() / 2; x < getWindowWidth() / 2; x += 0.5f ) {
		gl::vertex( getWindowCenter() + Vec2f( x, waveHeight * sin( x * mPhaseAdjust * audioToVisualScale ) ) );
	}
	glEnd();
    
// Text ab hier
#if defined( CINDER_COCOA_TOUCH )
	std::string normalFont( "Arial" );
    //	std::string boldFont( "Arial-BoldMT" );
    //	std::string differentFont( "AmericanTypewriter" );
#else
	std::string normalFont( "HelveticaNeue-Bold" );
    //	std::string boldFont( "Arial Bold" );
    //	std::string differentFont( "Papyrus" );
#endif
    
	// this does a complicated layout
	TextLayout layout;
	layout.clear( ColorA( 0.2f, 0.2f, 0.2f, 0.2f ) );
	layout.setFont( Font( normalFont, 24 ) );
	layout.setColor( Color( 1, 1, 1 ) );
     //       console() << mPhase << endl;
	layout.addLine(boost::lexical_cast<std::string>(mFreqTarget));
	Surface8u rendered = layout.render( true, PREMULT );
	mTexture = gl::Texture( rendered );
    
//    glClearColor( 0.1f, 0.1f, 0.1f, 1.0f );
//	glClear( GL_COLOR_BUFFER_BIT );
	gl::setMatricesWindow( getWindowSize() );
	gl::enableAlphaBlending( PREMULT );
	gl::color( Color::white() );
	gl::draw( mTexture, Vec2f( 10, 20 ) );
    
}

CINDER_APP_BASIC( AudioGenerativeApp, RendererGl )
